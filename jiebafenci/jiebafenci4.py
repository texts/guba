#! /usr/bin/env python
#coding=utf-8
"""This function is used for cut sentences into words we need """
import jieba
import jieba.posseg
import pandas as pd
import pickle #使用pickle方式存储数据

def myfenci(datafilepath,storefilepath_pkl,storefilepath_txt):
    jieba.load_userdict('C:/Users/ZHU/guba/jiebafenci/dic/finance.txt') #添加用户词典
    jieba.load_userdict('C:/Users/ZHU/guba/jiebafenci/dic/stockwords.txt')
    strings = pickle.load(open(datafilepath,'r'))#使用pickle导入
    
    stw = [line.strip().decode('utf-8') for line in open('.//dic/stopwords.txt').readlines()]#读取停词表，读入的每一行进行解码
    string_list_txt = [0 for x in range(len(strings))]
    string_list_pkl = [0 for x in range(len(strings))]
    without_stop = []
    i = 0
    for string in strings:
        seg = jieba.cut(string.decode('utf-8'))#对每篇文章进行分词，分词时首先进行解码转化成unicode格式
        without_stop = list(set(seg)-set(stw))#去除停词等，结果存成列表
        string_list_pkl[i] = without_stop #将去完停词后的所有文章存入一个列表，后面用pkl方式储存
        without_stop = ','.join(without_stop)#去完停词的结果转化成string格式
        string_list_txt[i] =without_stop.encode('utf-8')#将所有文章存入一个列表，输出时直接编码成utf-8形式，后面用txt方式储存
        i = i + 1


    f1 = open(storefilepath_pkl,'w') #pkl方式储存
    pickle.dump(string_list_pkl,f1)
    f1.close()
    

    string_list_txt = '\n'.join(string_list_txt)#将包含所有文章的列表存成字符串格式以‘/’分割，并写入文件
    f2 = open(storefilepath_txt,'w')
    f2.write(string_list_txt)
    f2.close()
    
myfenci('article.pkl','string.pkl','string.txt')