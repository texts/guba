#! /usr/bin/env python2.7
#coding=utf-8
"""This function is used for cut sentences into words we need """
import jieba
import jieba.posseg
#string = '这是用来测试分词函数的句子。！'
def myfenci(string):
    stw =  [line.strip().decode('utf-8') for line in open('stopwords.txt').readlines()]
    string_list = []
    without_stop = []
    
    seg = jieba.cut(string)
    for i in seg:
        string_list.append(i)
    without_stop = list(set(string_list)-set(stw))
    

    f1 = open('words_without_stw.txt','w')
    for i in without_stop:
        f1.write(i.encode('utf-8')+' ')
    f1.close()
#myfenci(string)
