#! /usr/bin/env python
#coding=utf-8
"""get articles"""
import pandas as pd
import pickle #使用pickle方式存储数据

df = pd.read_csv('szzs_1204_00_29.csv',header=None)#使用pandans读入数据
strings = df[5] #取第五列article
for i in range(len(strings)):
    strings[i] = strings[i].strip()
strings = df[2] + '。' + strings  #加入标题

f = open('article.pkl','w')#使用pkl方式储存
pickle.dump(strings,f)
f.close()

strings = '\n'.join(strings)#转化成string形式并用换行符分割
f1 = open('article.txt','w')#使用txt储存方便查看
f1.write(strings)
f1.close()