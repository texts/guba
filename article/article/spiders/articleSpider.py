__author__ = 'ZHU'
# -*- coding: utf-8 -*-
import scrapy
from scrapy import Spider, Selector
from scrapy.http import Request
from article.items import ArticleItem


class ArticleSpider(Spider):
    name = 'table'
    allowed_domains = ['eastmoney.com']
    start_urls = ['http://guba.eastmoney.com/list,szzs.html']

    def start_requests(self):
        pages = []
        u1 = 'http://guba.eastmoney.com/list,szzs_'
        u2 = '.html'
        for  i in range(1,2):
            newpage = Request(u1+str(i)+u2)
            pages.append(newpage)
        return pages
    def parse(self,response):
        sel = Selector(response)
        articles = sel.xpath('//div[@class="articleh"]')
        for each in articles:
            articleUrl = 'http://guba.eastmoney.com'+ each.xpath('.//span[3]/a/@href').extract()[0]
            #print articleUrl
            yield Request(articleUrl, callback = self.parse_item)
    def parse_item(self,response):
        sel = Selector(response)
        item = ArticleItem()
        item['Title'] = sel.xpath('//div[@id="zwconttbt"]/text()').extract()
        item['Totalread'] = sel.xpath('//*[@id="zwlist"]/script[1]/text()').re('\d+')[0]
        item['Comment'] = sel.xpath('//*[@id="zwlist"]/script[2]/text()').re('\d+')[1]
        item['Auther'] = sel.xpath('//div[@id="zwconttbn"]/strong/a/text()').extract()
        item['Date'] = sel.xpath('//div[@class="zwfbtime"]/text()').re('\d+')
        item['Article'] = sel.xpath('//div[@class="stockcodec"]/text()').extract()
        yield item

